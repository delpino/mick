## MICK - a RTOS by Kent del Pino
Minimum Inter-process Communication Kernel (MICK) is a small preemptive scheduler which provides some limited, yet effective features for coordinating inter-process dependencies and inter-process handovers. MICK is suitable for data routing (protocols) on small microcontrollers based on ARM's Cortex-M0, where resources such as RAM and flash memory is very limited. 
  
* Started in Valencia (2015) at [GeeksHubs](http://geekshubs.com) and developed further at [GreenLab](http://greenlab.org). Now MICK lives somewhere in England. 

While working on the MICK, my focus has also been on fast event-signal handling. Signals are set by, for example, Interrupt Service Routines (ISR). The collection of data, such as the receipt of data-packages on a UART is done by an ISR. When a packet of data items(a string) is received, the ISR can set a signal-flag via osSignalSet() and MICK will quickly activate the handler-task for further processing of the data-package at an appropriate priority level.

The MICK-Kernel's systick runs at a speed that is 4 times higher than the schedule-time's (task-shift) to lower the response time on signals, this also enhances the resolution on the clock(). The file DEF_YOUR_TASKS.h is an important part of the overall documentation regarding priorities settings and Task stack-sizes.

![MICK](MICK_execution_diagram.jpg)  

The code is written in highly efficient, low footprint C with very little ASM in readable form. Here is an example on code-style and by the example, one can see how kernel extracts the signal among signals. 

```c
/* Return next Signal in priority order. No-signal-found, zero(0) is returned. */  
static SIGNAL_LEN_T osSignalGetNext(SIGNAL_LEN_T signal) {

  register SIGNAL_LEN_T flag_s = ISR_SIGNAL_ABSOLUTE;

  while ((flag_s) && !(signal & flag_s)) {  
    flag_s = (flag_s << 1) & IRS_SIGNAL_MASK;  
   }  
  return flag_s;  
} /* osSignalGetNext */  
```

Since the MICK is meant for micros with constrained resources, the Minimum Inter-process Communication functionality only make use of a few CPU registers for pointer transfer, such as: `taskID`, `signal_len` and `massageOption`. The Inter-process Communication takes place as a kernel(OS) service. All this is well documented in the source.  

An example of use with other code, makefile etc. can be seen here: [theBase](https://github.com/kentdelpino). When using MICK, we need one old-fashioned main(), within this function we initialize and start the MICK RTOS scheduler. Here is also an example:
```c
int main(void) {
 /* Initialization of some more ,, if needed */
  SystemCoreClockUpdate();  

 /* First the Kernel and the the Tasks */
  osKernelCreate(); 
 /* JSON objects incoming */
  osThreadCreate(STDIN_JSON_ID, task_stdin_json, STDIN_JSON_STACK, STDIN_JSON_PRIO);
 /* On queries, We respond on STDOUT */
  osThreadCreate(STDOUT_JSON_ID, task_stdout_json, STDOUT_JSON_STACK, STDOUT_JSON_PRIO);
 /* The actual application */
  osThreadCreate(OUR_APP_ID, task_application, OUR_APP_STACK, OUR_APP_PRIO);

 /* Is there more to say! */
  osKernelRun();
} 
```

The source code is meant to be compiled with the [GNU ARM Embedded Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm) and it uses ARM specific directives. 

For inspiration next time, maybe the [Super Simple Tasker](http://www.embedded.com/design/prototyping-and-development/4025691/Build-a-Super-Simple-Tasker) (SST)  :-)
