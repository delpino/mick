
 /* This file contains code, originally from Shinichiro Nakamura, 
  * but has later been change. Original version 1.1
  */

/**
 * @file tcblink.c
 * @brief Link List for Task Context Blocks.
 * @details UOS-LPC800.
 * @author Copyright(C) CuBeatSystems
 * @author Copyright(C) Shinichiro Nakamura
 */

#include "tcblink.h"


void tcblink_init(TCBLINK *tcblink) {

 /* 
  * All ->next and ->prev should be TCB_NULL 
  */

} /* tcblink_init */


/* Inline(r) to release a Thread Control Block (TCB) from a chain of TCBs */
static inline void tcbReleaseFromChain(TCB *theOne)
{
  TCB *p = theOne->prev;
  TCB *n = theOne->next;

  if (p != TCB_NULL) {
     p->next = theOne->next;
   }

  if (n != TCB_NULL) {
    n->prev = theOne->prev;
   }

 /* Released */ 
  theOne->prev = TCB_NULL;
  theOne->next = TCB_NULL;
} /* tcbReleaseFromChain */ 


void tcblink_push(TCBLINK *tcblink, TCB *tcb) {
  
  TCB *root = &(tcblink->root);
  TCB *work = root;
 // Just check   
  if (tcb == TCB_NULL)
    return;
 // Find free slot, to insert it
  while (work->next != TCB_NULL)
    work = work->next;
 // Insert it
  work->next = tcb;
  tcb->prev = work;
 // There is a new free slot 
  tcb->next = TCB_NULL;
} /* tcblink_push */


TCB *tcblink_pull_with_priority(TCBLINK *tcblink) {

  unsigned int highest_priority = 0;  /* Init as the lowest */
 // of type PRIORITY_T 
 
  TCB *highest_tcb = TCB_NULL;
  TCB *w = tcblink->root.next;

 /* Run through the entire list of Threads */
  while (w != TCB_NULL) {
    if (highest_priority < w->thread.info.priority) {
      highest_priority = w->thread.info.priority;
      highest_tcb = w;
     }
    w = w->next;
   }

  if (highest_tcb != TCB_NULL) {
    tcbReleaseFromChain(highest_tcb);
    return highest_tcb;
   }

  return TCB_NULL;
} /* Pull_with_priority */


/* Pull thread/task with ID, if state is correct */
TCB *tcblink_pull_id_and(TCBLINK *tcblink, THREADID_T id, STATE_T state) {

  TCB *w = tcblink->root.next;

  while (w != TCB_NULL) {
   /* Is it the one */
    if (w->thread.info.tid == id) {
     /* Does the state fit */
      if (w->thread.info.state == state) {
        tcbReleaseFromChain(w);
        return w;
       }
      else 
        return TCB_NULL;
     }
   /* Try next in chain */  
    w = w->next;
   }

  return TCB_NULL;
} /* tcblink_pull_id_and */



TCB *tcblink_pull_with_tcb(TCBLINK *tcblink, TCB *tcb) {

  TCB *w = tcblink->root.next;
  
  while (w != TCB_NULL) {
    if (w == tcb) {
      tcbReleaseFromChain(w);
      return w;
     }
   w = w->next;
  }
  
  return TCB_NULL;
} /* tcblink_pull_with_tcb */


TCB *tcblink_pull_state(TCBLINK *tcblink, STATE_T state) {

  TCB *w = tcblink->root.next;
  
  while (w != TCB_NULL) {
    if (w->thread.info.state == state) {
      tcbReleaseFromChain(w);
      return w;
     }
    w = w->next;
   }
  
  return TCB_NULL;
} /* tcblink_pull_state */


void tcblink_list(TCBLINK *tcblink, TCBLINK_LIST_CALLBACK callback) {

  TCB *w = &(tcblink->root);
  
  do {
    TCB *next = w->next;
    callback(w);
    w = next;
   } while (w != TCB_NULL);
} /* tcblink_list */




/* NOT used, is for Round-Robin mode */
/*
TCB *tcblink_pull(TCBLINK *tcblink) {
    TCB *root = &(tcblink->root);
    TCB *q1st = root->next;
    TCB *q2nd = (q1st == TCB_NULL) ? TCB_NULL : q1st->next;
    if (q1st == TCB_NULL) {
        return TCB_NULL;
    }
    root->next = q2nd;
    if (q2nd != TCB_NULL) {
        q2nd->prev = root;
    }
    q1st->prev = TCB_NULL;
    q1st->next = TCB_NULL;
    return q1st;
}
*/