/**************************************************************************
 *                                    Link: www.linkedin.com/in/kentdelpino
 *                                    Author: Kent DEL PINO
 * File: mick.h
 * Used with: GNU Arm Embedded Toolchain
 * Version:   0.4.3
 * Date:      22 APR. 2018
 *
 *
 *                      API to the mimimum kernel.
 *       -- Minimum Inter-process Communication Kernel (MICK) --  
 *
 * The algorithm of MICK is priority driven time-slicing on a preemptive 
 * scheduler base, a more "Round-Robin" behavior can be achieved by giving 
 * the tasks equal priority. The MICK is made to support the ARM Cortex-M0+ 
 * core, a smaller MCU-core, so data-unit(int) is 32 bits wide. This 
 * architecture is equally as good, in terms of CPU clock cycles, of loading 
 * and storing 8, 16 and 32 bits-words and size matter, so therefore the 
 * approach is: What is needed in data-size, not more. 
 * 
 * The file _DEF_YOUR_TASKS.h is also an important piece of document for 
 * the user of this software, in addition to the comments, in this file. 
 * 
 * 
 * -- This source-code is released into the public domain, by the author --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABI-
 * LITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT 
 * SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 *
 * The author of this material /source-code kindly ask all users and distri
 * butors of it, to leave this notice / text in full, in all copies of it, 
 * for all time to come.
 */

#ifndef MICK_H
#define MICK_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include <stdbool.h>
#include <time.h> /* and <stddef.h> */
#include "_DEF_YOUR_TASKS.h"


/* The SYS_TICK_FREQ determines how often the scheduler interrupts the current 
 * running thread, to make a task-shift. Good balance in the system is needed, 
 * not too many task-shifts (waste of resources) and at the same time a required 
 * shot System reaction-time.
 * 
 * Set SYS_TICK_FREQ to 200, 400, 600 or 800(even 100s). SYS_TICK_FREQ at 400Hz, 
 * give as minimum a task-shift(context shift) every 2.5mS. The physical SYS_TICK 
 * _COUNTER runs at a speed that is 4 times higher to lower the response-time on 
 * signals(HW-events) and to enhance the resolution on CLOCK/TIME. 
 *
 * NOTE: The newLib macro CLOCKS_PER_SEC is NOT the same as this SYS_TICK_FREQ. 
 *       A Thread is NOT guaranteed to run the scheduled time, there are pri-
 *       orities and Nested behavior.  
 */
#define SYS_TICK_FREQ         200
#define SYS_TICK_COUNTER      (SYS_TICK_FREQ * 4) /* Do NOT alter this ratio */
                                                  /* -Just an idea to work on. 
                                                   * -Let the TICK_COUNTER run
                                                   * -8 times faster than TICK_
                                                   * FREQ and generally lower  
                                                   * the schedule/TICK_FREQ. */


/* The ratio between POSIX CLOCKS_PER_SEC and this firmwares SYS_TICK_FREQ */
/* POSIX standard defines CLOCKS_PER_SEC value is 1000000. HOWEVER, the cur-
 * rent value for ARM/THUMB in our environment, is 100. See machine/time.h. 
 */
#define RATIO_IN_CLOCK        ( (uint32_t)((SYS_TICK_COUNTER) / CLOCKS_PER_SEC) )


/* Using milliseconds in a system that use Ticks, for example as:
 *       osDelay(TICKS_MILLISEC(500)); Give a half second break.  
 *
 * NOTE: Good for constant, as in Known at compile-time.
 */
#define TICKS_MILLISEC(M_SEC)  ( (uint32_t) ((((float)M_SEC / 1000)) * SYS_TICK_COUNTER) )



/* For test purpose. Overwrite different Stacks with known content. */
#define INITIALIZE_TEST_STACKS

#if defined (INITIALIZE_TEST_STACKS)

  /* Monitor Main Stack Pointer (MSP), the HW and SW Interrupt Stack. 
   * This functionality is dependent on Linker information, we need:
   *   __StackLimit and __StackTop
   * 
   *            WORK IN PROGRESS 
   */
  #define MONITOR_INTERRUPT_STACK

  /* Overwrite all memory areas, used as local process stacks(a task stack) 
   * with the value of INITIALIZE_TASK_STACK_WITH.
   * If not defined, the individual process stacks will be overwritten with the 
   * process number (thread ID) of its corresponding task. This can be used while 
   * while debugging.
   */
  //#define INITIALIZE_TASK_STACK_WITH    0x0000

#endif /* INITIALIZE_TEST_STACKS */


/* This Thread/Task is meeting its deadlines, if it often 'ends' itself by 
 * osThreadYield() */
typedef uint8_t DEADLINE_T;
/* Thread priority; Fixed at link time */
typedef uint8_t PRIORITY_T;
/* Type for Thread states */
typedef uint8_t STATE_T;
/* Thread ID-numbers; Fixed at link time */
typedef uint8_t THREADID_T;  

/* Thread info in bytes(*4) stored as one word(uint32_t) aligned */
typedef struct {
  DEADLINE_T success;
  PRIORITY_T priority;
  STATE_T state;
  THREADID_T tid;
} THREAD_WORD_INFO_T __attribute__ ((aligned (4))); 



/**************************************************************************
 * SIGNALs: Flags set by Handlers and Interrupt Service Routines(ISR) to 
 * make the kernel aware of events, such as sysTickTime Interrupt and re-
 * ception of data packets on stdin or other IOs. 
 * Flag-priority is: the higher bit-number the lower process-priority. 
 * 
 * osEvent is the CMSIS term.
 */

/* SIGNAL_LEN_T: Combined flag-holder for SIGNALs and can contain length 
 * of String, when used for MASSAGEs*/
typedef volatile int SIGNAL_LEN_T;


/* SIGNAL constants; Consider this as Hard-Wired */
typedef enum {
  MASSAGE_NULL            = 0x00000000,
 /* Error = -1 */
  MASSAGE_ERROR           = (0x80000000 + 0x7fffffff),
 /* Maximum length of massage 4095 bytes */
  MASSAGE_LEN_MASK        = 0x00000fff,

 /* Bit[30]: SysTickTimer Signal, normal scheduler time. */
  IRS_SIGNAL_SYSTICK      = 0x40000000, 

 /* READY, all Signals/Events are accepted. */
  IRS_SIGNAL_READY        = 0x20000000,
 /* Initialization: Doing start-Up only SYSTICK-event are accepted */
  IRS_SIGNAL_INIT         = 0x10000000,

 /* Mask-out 16 Signals, grupped in four nibbles, or like: 
  *                         bit[12] to bit[27] = 0x0ffff000 */
  IRS_SIGNAL_MASK         = ((0xf << 12) | (0xf << 16) | (0xf << 20) | (0xf << 24)), 
 /* Mask-out Signals */
  IRS_SIGNAL_KERNEL_MASK  = (IRS_SIGNAL_MASK | IRS_SIGNAL_INIT | IRS_SIGNAL_READY),
 /* Mask-out Counter: Start-up delay/countdown, until kernel is READY for signals */
  IRS_SIGNAL_INIT_C_MASK  = MASSAGE_LEN_MASK,  /* = 0x00000fff */

/* -> Bit[12]..[27]: High bit-number, lower the process-priority -> */

/* Group/nibble #0 (0xf << 12) */
 /* Cortex-M Pattern Match or State Configurable Timer */
  ISR_SIGNAL_ABSOLUTE     = (1 << 12),   /* The first and Hard-Wired */
 /* ADC, Comparator or DAC */
  ISR_SIGNAL_ANALOG       = (1 << 13),
  ISR_SIGNAL_TIMER        = (1 << 14),
 /* System Power and oscillator(PLL) monitor */
  ISR_SIGNAL_POWER_CTRL   = (1 << 15),

/* Group/nibble #1 (0xf << 16)*/
 /* Shared flag, if reception from multiple SPI channels */   
  ISR_SIGNAL_SPI_RXDATA   = (1 << 16),
 /* RX_COMPLETED on STDIN/Com1/UART0 */
  IRS_SIGNAL_TTYS0_RXD    = (1 << 17),   /* IRS_SIGNAL_STDIN */
 /* Shared flag on other ComX/UARTs */ 
  IRS_SIGNAL_UARTS_RXD    = (1 << 18),   /* IRS_SIGNAL_RS485_RXD */
 /* I2C completed, shared flag, if more than 1 I2C-channel is used */
  ISR_SIGNAL_I2C_RXDATA   = (1 << 19),
  
/* Group/nibble #2 (0xf << 20) */
 /* For notifications to higher application levels, form Handlers */
  HANDLER_SIGNAL_0        = (1 << 20),
  HANDLER_SIGNAL_1        = (1 << 21),
  HANDLER_SIGNAL_2        = (1 << 22),
  HANDLER_SIGNAL_3        = (1 << 23),
  
/* Group/nibble #4, COMPLETED-FLAGs */
 /* SPI (shared if) */   
  ISR_SIGNAL_SPI_COMP     = (1 << 24),
 /* UART0/STDOUT */
  IRS_SIGNAL_TTYS0_COMP   = (1 << 25),
 /* Other UARTs */ 
  IRS_SIGNAL_UARTS_COMP   = (1 << 26),
 /* I2C (shared if) */
  ISR_SIGNAL_I2C_COMP     = (1 << 27),
  
} SIGNAL_LENGTH_CONST_T;


/**************************************************************************
 * API. Some functions a based on Supervisor Calls (software-interupts) and 
 * some are made as calls directly into the kernel-world, in thread-mode and 
 * on thread-stack, these can be temporarily disable interrupts, sorry.
 */

      /* Kernel administration calls. */

/* FIRST create/init the Kernel, THEN add Threads/Tasks. */
void osKernelCreate(void);

/* Prepare a tread for running, the threadID and threadPriority are constants 
 * defined by user. Define each threadID as a unique number (from 1 and 
 * consecutively up. threadPriority greater than 1 (the higher the number the 
 * higher the priority(max. 128). The thread Stack needed, is expressed in
 * words of 4 bytes and Stack size is min. 16. 
 */
void osThreadCreate(THREADID_T threadID, void (*thread)(void), uint32_t stackSize, PRIORITY_T priority);

/* You start the kernel, after initialisation by osKernelInit() and creation/
 * set-Up of all the Threads/tasks, There is no return to main(). */
void osKernelRun(void);

/* Terminate execution of a thread, but can NOT free for allocated memory. */
 /* void osThreadTerminate(threadID_t id); */

/* Yield thread execution and pass control to another READY thread */
void osThreadYield(void);

/* Wait for timeout, time is in ticks(one fourth of sysCounter), if milli-
 * seconds is wanted, used the marcro TICKS_MILLISEC(M_SEC) */
void osDelay(uint32_t ticks);

/* osWait() We Do not have a Wait-function, see osThreadRecv() */



/*             --  Inter-Process(Thread) Communication (IPC)  --
 *               --  Is also used to receive Kernel SIGNAL  --
 * 
 * IPC caution: There is no control, on the parameters handed-over (-: at 
 * all :-). The Send-function are blocking until the receiver request data-
 * input via the Recv-function. 
 * 
 * If STOP_IPC_MASSAGE_PUSH is defined, then on sending Massage or Signal, 
 * the receiver will, receive a pointer directly to the sender's memory 
 * area. Therefore the receiver have to grab/copy the data quickly.
 */
 // NOT define STOP_IPC_MASSAGE_PUSH


/* Send notification about a ready Massage or Byte to other task/thread with 
 * ID different then 0(the kernel). While sending a Massage, the signal_len 
 * parameter is equal to length of string or numbers of elements(len >= 1) 
 * that the pointer(massage), is pointing to.
 * While sending a value/Byte, the pointer(massage) is 0 and signal_len para-
 * meter points to the value-holder to be transmitted.
 * 
 * On Error: signal_len will be 0. (In later versions it will be -1).
 */ 
void osThreadSend(THREADID_T recvID, SIGNAL_LEN_T *signal_len, uint8_t **massage);


/* On receiving a notification, signal_MAXLEN is set equal to the length of the 
 * string (numbers of bytes) ready to be taken, if any. On receiver request the 
 * signal_MAXLEN can be set to the size(-2) of the receiving buffer (massage_ptr).  
 * 
 * NOTE: If STOP_IPC_MASSAGE_PUSH is defined, the massage_ptr will point to 
 * the sender's data-buffer.
 * 
 * If only a Signals is wanted, then set massage_ptr to null(0) and on received 
 * notification signal_maxlen will hold a value/signal. Upon receipt, the 
 * senderID contains task-number of sender.
 * 
 * Open for Kernel-Signals or Values(no strings) from other tasks(IPC), do as:
 * 
 *           senderID = 0xFF;
 *           osThreadRecv(&senderID, &signalholder, 0);
 * 
 * For receiving Massages(string), Values and/or Kernel-Signals you can do as:
 * 
 *           senderID = 0xFF;
 *           maxLength = TTY_RX_MAX;
 *           massReadyPtr = ttyS0_RX; 
 * 
 *           osThreadRecv(&senderID, &maxLength, &massReadyPtr);
 */
void osThreadRecv(THREADID_T *sendID, SIGNAL_LEN_T *signal_maxlen, uint8_t **massage_ptr);




/****************************************************************************
 * POSIX clock() and time(), Machine-timeon ARM Cortex-M0+
 * 
 * The functions clock() and time() is defined here. This project focus on 
 * POSIX/Linux time, 1 January 1970. For now the second-counter is of type 
 * long,  * which result in the 2038 problem. In the embedded ARM GCC, 
 * _CLOCK_T_ is defined as unsigned long (32 bit) and _TIME_T_ long (32 bit).
 * 
 * See the functions before using them and for user inspiration: 
 *                                http://www.catb.org/esr/time-programming/
 */


/* Return Ticks(POSIX CLOCKS_PER_SEC) since start/reset, based on GCC for 
 * ARM/THUMB, meaning that a Second = clock() / CLOCKS_PER_SEC. 
 */
clock_t clock(void);  

//time_t time(time_t *timer);



/****************************************************************************
 * High-resolution timestamps, absolutely proprietary, better than 0,1 milli-
 * second accuracy with a variation of 1 percent on the main clock (RC based).
 * 
 * Limit: Only works on times LESS than a second, it is the user's responsi-
 * bility to ensure this time-window. 
 * 
 * Note: depending on the a ARM Cortex-M 24bits SysTick-timer, running maximum 
 * 50MHz(then Core-speed will be 100MHz). 
 */ 
typedef struct {
  uint16_t ttHard;  /* Part for ARM SysTick->VAL(16bit, down-counting) */
  uint16_t ttSoft;  /* Part that hold kernel.time.ticks   */ 
} TICK_PARTS_T; 

/* Higher-resolution time-holder, as one(1) word or two parts. 
 */
typedef union {
  uint32_t tt32;
  TICK_PARTS_T tt;
} TICK_TACKS_T;


/* Timestamp in (u)microsecond, based on the 24-bits tick-counter in the 
 * Cortex-M, which normally runs on SystemCoreClock/ 2. on LPC series. 
 */
void ttStamp(TICK_TACKS_T *ttp);


/* The difference between 'New' and 'then', in hardware-clocks, only 
 * measured over time LESS than a second. 
 */ 
uint32_t ttBetween(TICK_TACKS_T ttNew, TICK_TACKS_T ttThen, bool inMicroSec);


/**************************************************************************
 * Following API-calls is to be used by/within Interrupt Service Routines 
 * and 
 */


/* Set Signal-flags for Interrupt Service Routine (ISR) call to Handler. 
 * On success the signal-flag is returned, on failure zero(0) is returned.
 */ 
SIGNAL_LEN_T osSignalSet(SIGNAL_LEN_T signal);



#ifdef __cplusplus
}
#endif

#endif /* MICK_H */