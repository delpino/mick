/**************************************************************************
 *                                    Link: www.linkedin.com/in/kentdelpino
 *                                    Author: Kent DEL PINO
 * File: mick.c
 * Used with: GNU Arm Embedded Toolchain
 * Version:   0.4.3
 * Date:      22 APR. 2018
 *
 *
 *       -- Minimum Inter-process Communication Kernel (MICK) --  
 *
 * The algorithm of MICK is priority driven time-slicing on a preemptive 
 * scheduler, a more "Round-Robin" behavior can be achieved by giving the 
 * tasks equal priority. The MICK is made to support the ARM Cortex-M0+ 
 * core, a smaller MCU-core, so data-unit(int) is 32 bits wide. This 
 * architecture is equally as good, in terms of CPU clock cycles, of loading 
 * and storing 8, 16 and 32 bits-words and size matter, so therefore the 
 * approach is: What is needed, not more. 
 * 
 * 
 * -- This source-code is released into the public domain, by the author --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABI-
 * LITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT 
 * SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 *
 * The author of this material /source-code kindly ask all users and distri
 * butors of it, to leave this notice / text in full, in all copies of it, 
 * for all time to come.
 */


/**************************************************************************
 *  - Some very importen documents, while programing a Kernel. - 
 *
 * ARM GCC Inline Assembler Cookbook: 
 *   http://www.ethernut.de/en/documents/arm-inline-asm.html
 * 
 * A Beginner’s Guide on Interrupt Latency - and Interrupt Latency:  
 *   https://community.arm.com/docs/DOC-2607
 */


#include "mick.h"
#include "tcb.h"
#include "tcblink.h"

/* Our MCU */
#include "chip.h"

#ifndef STOP_IPC_MASSAGE_PUSH
#include <string.h>
#endif


/* Kernel clock/calendar/time */
typedef struct {
  time_t seconds;
  uint32_t ticks;
} MICK_TIME_T; 


/* Memory space for ALL the Thread-Stacks(including the Kernel) */
static volatile unsigned int all_stack_mem[OS_STACK_NEEDED];
/* Pointer to last Thread-Stack-Top allocated */
static unsigned int *lastStackInit = (unsigned int *) &all_stack_mem[0];
/* How many HW-sysTicks-clocks for one(1) micro-second */
static uint32_t microAmountOfClocks;


/* Kernel structure */
typedef volatile struct {
  TCB *tcb_current;       /* OFFSET #0 -> to current process */  
  TCBLINK tcblink_ready;
  TCBLINK tcblink_sleep;
  TCBLINK tcblink_block;
  MICK_TIME_T time;
  SIGNAL_LEN_T signals;
} kernel_t __attribute__ ((aligned (8))); 

/* Offset into kernel_t struct, use in ASM(inline) */
#define OFS_TCB_CURRENT       (0)  // Offset in -words-
/* Kernel main structure */
static kernel_t kernel; 
/* A Task Control Block for each of the Threads/Tasks */
static TCB tcb_list[NUM_OF_THREAD];


/* Task operation-states */
typedef enum {
 /* Not Created */
  STATE_NON,
 /* A thread that is ready to execute */
  STATE_READY,
 /* A task on pause, for requested time */
  STATE_SLEEP,
 /* Sending task that is blocked, while waiting for a receiver */
  STATE_BLOCKED_SENDER,
 /* Receiver task that is blocked, until there is a signal or a sender */
  STATE_BLOCKED_RECEIVER,
} STATE_TO_BE_IN;



/**************************************************************************
 * Signal handling.
 * Table of Handler-IDs(Thread-IDs), The IDs are assigned in the file:
 *       _DEF_YOUR_TASKS.h 
 * 
 * If a Handler is zero(0), it is desabled. */
const THREADID_T SIGNAL_HANDLER_NO [] = { 
  SIGNAL_HANDLER_00, SIGNAL_HANDLER_01, SIGNAL_HANDLER_02, SIGNAL_HANDLER_03,
  SIGNAL_HANDLER_04, SIGNAL_HANDLER_05, SIGNAL_HANDLER_06, SIGNAL_HANDLER_07,
  SIGNAL_HANDLER_08, SIGNAL_HANDLER_09, SIGNAL_HANDLER_10, SIGNAL_HANDLER_11,
  SIGNAL_HANDLER_12, SIGNAL_HANDLER_13, SIGNAL_HANDLER_14, SIGNAL_HANDLER_15
};


/* Return next Signal in priority order. No-signal-found, zero(0) is 
 * returned. */
static SIGNAL_LEN_T osSignalGetNext(SIGNAL_LEN_T signal) {

  register SIGNAL_LEN_T flag_s = ISR_SIGNAL_ABSOLUTE;

  while ((flag_s) && !(signal & flag_s)) {
    flag_s = (flag_s << 1) & IRS_SIGNAL_MASK;
   }

  return flag_s;
} /* osSignalGetNext */


/* Return Thread-ID assigned to the Event, this signal-type. */
static THREADID_T osSignalToThread(SIGNAL_LEN_T signal) {

  register SIGNAL_LEN_T flag_s = ISR_SIGNAL_ABSOLUTE;
  register int index = 0;
  
 // Align the signal-flags, while counting index Up
  while ((flag_s) && !(signal & flag_s)) {
    flag_s = (flag_s << 1) & IRS_SIGNAL_MASK;
    index++;
   }

  if (flag_s)
    return SIGNAL_HANDLER_NO[index];
  
  return 0;
} /* osSignalToThread */


/* Clear Signal-flag. While manipulation with it, the interrupt capability is 
 * disabled. Only to be used by the Kernel.
 */ 
STATIC INLINE void osSignalClear(SIGNAL_LEN_T signal)
{
  __disable_irq();
  kernel.signals &= ~(IRS_SIGNAL_MASK & signal);
  __enable_irq();
} /* osSignalClear */ 


/* Set Signal-flags for Interrupt Service Routine (ISR) call to handler. 
 * While manipulation with the Interrupt Signals, we disable the Interrupt 
 * capability. 
 * 
 * On success the signal-flag is returned, on failure zero(0) is returned.
 */ 
SIGNAL_LEN_T osSignalSet(SIGNAL_LEN_T signal) {

  SIGNAL_LEN_T theRes = 0;

  __disable_irq();
 /* Only when the kernel is READY to receive */
  if (IRS_SIGNAL_READY & kernel.signals) {
    kernel.signals |= (IRS_SIGNAL_MASK & signal);
    theRes = (IRS_SIGNAL_MASK & signal);
   }
  __enable_irq();

  return theRes;
} /* osSignalSet */ 



/**************************************************************************
 * API based on directly read-outs on Kernel-time variables.
 */ 


/* Record time information, in Tick-tacks. 
 * 
 * NOTE: TICK_TACKS_T is Hardware time
 */
void ttStamp(TICK_TACKS_T *ttp) {

  __disable_irq();
  ttp->tt.ttHard = (uint16_t) SysTick->VAL; 
  ttp->tt.ttSoft = (uint16_t) kernel.time.ticks; 
  __enable_irq(); 
} /* ttStamp */


/* Tick-tacks between the two. */
uint32_t ttBetween(TICK_TACKS_T ttNew, TICK_TACKS_T ttThen, bool inMicroSec) {

  register uint32_t diff;
  register uint32_t hwSysTick = ((0x00FFFFFFUL & SysTick->LOAD) +1UL);

 /* Difference in soft-ticks(counting UP) */
  if (ttThen.tt.ttSoft > ttNew.tt.ttSoft) 
    diff = (SYS_TICK_COUNTER - ttThen.tt.ttSoft) + ttNew.tt.ttSoft; 
  else 
    diff = ttNew.tt.ttSoft - ttThen.tt.ttSoft;

 /* Scale up the soft-ticks by hard-counts */
  if (diff != 0)
    diff = (uint32_t) diff * hwSysTick; 

 /* Difference in hard-counts (counting DOWN) */
  if (ttNew.tt.ttHard <= ttThen.tt.ttHard)
    diff += (uint32_t) (ttThen.tt.ttHard - ttNew.tt.ttHard);  
  else 
    diff -= (uint32_t) (ttNew.tt.ttHard - ttThen.tt.ttHard); 

  if (inMicroSec)
    return (uint32_t) diff / microAmountOfClocks;

  return diff;
} /* ttBetween */


/* Return machine run-time in (adjusted) POSIX Ticks 
 * 
 * NOTE: Based on MICK_TIME_T - System time 
 */
clock_t clock(void) {

  MICK_TIME_T sysTicksFromStart;
  uint32_t workNum;

 // Get the sys-time in seconds + fraction-part 
  __disable_irq();
  sysTicksFromStart.seconds = kernel.time.seconds;
  sysTicksFromStart.ticks = kernel.time.ticks;
  __enable_irq(); 

 // Calculated all the POSIX ticks/clocks from seconds
  workNum = (uint32_t) sysTicksFromStart.seconds * CLOCKS_PER_SEC; 
 // Add the fraction of a second
  return (clock_t) (workNum + (uint32_t) sysTicksFromStart.ticks / RATIO_IN_CLOCK);
} /* clock */


/* time_t time(time_t *timer); */



/**************************************************************************
 * API based on Supervisor Calls (software-interupts)
 * 
 * Good, when naming API-functions:-)
 * https://www.keil.com/pack/doc/CMSIS/RTOS/html/group___c_m_s_i_s___r_t_o_s___thread_mgmt.html
 *
 * Good doc on SV Calls:-)
 * https://falstaff.agner.ch/2013/02/18/cortex-m3-supervisor-call-svc-using-gcc/
 */

/* System Calls that result in Pending Request */
#define SVCFUNC_START         (0x20) /* Do NOT change */
#define SVCFUNC_SLEEP         (0x00) 
#define SVCFUNC_YIELD         (0x01) /* From 0x01 to 0x1F */  
#define SVCFUNC_SEND          (0x02)  
#define SVCFUNC_RECV          (0x03)  


/* Macro to start-Up a Pending Interrupt Request */
#define REQUEST_PENDSV()            do { SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk; } while (0)


/* SVC with the given immediate [%0] and a NOP to insure a safe/aligned return address */
#define SVC(svcFunc) __asm volatile("svc %0  \n": : "I" (svcFunc)); \
                     __asm volatile("nop   \n")



__attribute__ ((used)) void osThreadSend(THREADID_T recvID, SIGNAL_LEN_T *signal_len, uint8_t **massage) {

 /* The gcc-kompiler for ARM automatic place parameters 1..4 in registers r0 to r3, but
  * we need to make sure, that the types are correct, so We work with/ use them here. 
  * 
  * NOTE: Problem seems to be octal numbers in word registers.
  */ 
  kernel.tcb_current->sys.sender.destID = (THREADID_T) recvID;  
  kernel.tcb_current->sys.sender.signall_p = (SIGNAL_LEN_T *) signal_len;  
  if (massage == 0)
    kernel.tcb_current->sys.sender.massage_pp = 0; 
  else // Or the address of the hand-over pointer
    kernel.tcb_current->sys.sender.massage_pp = (uint8_t **) massage; 
  
/* TODO: Test with NULL input */
  
 /* DO NOT send to the Kernel or outer space */
  if ((recvID == 0) || (recvID >= (THREADID_T) NUM_OF_THREAD)) {
   /* Just a check  */
    if (signal_len != (SIGNAL_LEN_T *) NULL)
      *signal_len = 0;  // -1; 
    return;
   }

  SVC(SVCFUNC_SEND);
} 


__attribute__ ((used)) void osThreadRecv(THREADID_T *sendID, SIGNAL_LEN_T *signal_maxlen, uint8_t **massage_ptr) {

  kernel.tcb_current->sys.receiver.sourID_p = (THREADID_T *) sendID; 
  kernel.tcb_current->sys.receiver.signall_p = (SIGNAL_LEN_T *) signal_maxlen; 
  if (massage_ptr == 0)
    kernel.tcb_current->sys.receiver.massageReady_pp = 0;
  else  
    kernel.tcb_current->sys.receiver.massageReady_pp = (uint8_t **) massage_ptr;   

/* TODO: Test with NULL input */
  
 /* Just a little check, IF !, just return */ 
  if ((sendID == (THREADID_T *) NULL) || (signal_maxlen == (SIGNAL_LEN_T *) NULL)) {
    return;
   }

  SVC(SVCFUNC_RECV);
} 


__attribute__ ((used)) void osDelay(uint32_t ticks) {

 // Doing the delay this value is steped-down by 4. 
  if (ticks < 4) 
    kernel.tcb_current->sys.sleep.count = 4;
  else 
    kernel.tcb_current->sys.sleep.count = ticks; 

  SVC(SVCFUNC_SLEEP);
} 


__attribute__ ((used)) void osThreadYield(void) {

  SVC(SVCFUNC_YIELD);
} 



/* A code-placed pointer, pointing to, Ha-ha, a data-placed data-structure. 
 * This code is for an ARM Cortex-M with thump address-methodes and on top 
 * of that We use GNU Link time optimization(LTO). 
 * 
 * WARNING: 
 * In Thump-mode bit[0] in an instruction/function address is 1, but this 
 * pointer should point to word-aligned data, with bit[0] = 0. 
 */
__attribute__ ((used, aligned (8))) void (* const kernel_ptr)(void) asm (".kernel_ptr") = (const void *) &kernel;


/* SVC handler(_in_c) 
 * The main purpose is to insure Task State-shift required by the Task itself, at 
 * a higher run-priority level then the one that is a result of SysTick(schedule).
 * 
 * svcArgs(in r0) is pointing to a Stack frame contains: r0, r1, r2, r3, r12, 
 * r14(LR), PC and xPSR and is addressed as:
 *          Stacked R0 = svc_args[0] or Stacked PC = svc_args[6].
 */
__attribute__ ((used, aligned (4))) void SVC_Handler_in_c(uint32_t *svcArgs) asm (".handler_in_c");

void SVC_Handler_in_c(uint32_t *svcArgs) {
 
 // Find SVC-function number, in caller SVC-instruction 
  register uint32_t svcNumber = ((uint8_t *)svcArgs[6])[-2];

  switch(svcNumber) { 
      
    case SVCFUNC_SLEEP:
      kernel.tcb_current->thread.info.state = STATE_SLEEP;
      REQUEST_PENDSV();
      break;
      
    case SVCFUNC_YIELD:
     // For now bla-bla
      kernel.tcb_current->thread.info.success = 0x08;  
      REQUEST_PENDSV();
      break;
      
    case SVCFUNC_SEND: 
      kernel.tcb_current->thread.info.state = STATE_BLOCKED_SENDER;
      REQUEST_PENDSV();
      break;
      
    case SVCFUNC_RECV: 
      kernel.tcb_current->thread.info.state = STATE_BLOCKED_RECEIVER;
      REQUEST_PENDSV();
      break;
      
    case SVCFUNC_START:
      __asm volatile(
        "  LDR   r3,=.kernel_ptr       \n\t"
        "  LDR   r1,[r3,#0]            \n\t"
      );
      
      __asm volatile(
        "  LDR    r0,[r1,%0]           \n\t"
        "  LDR    r2,[r0,%1]           \n\t"
        "  MSR    PSP,r2               \n\t"
        :
        : "I" (OFS_TCB_CURRENT), "I" (OFS_TCB_STACK)
       ); /* NOTE: OFS_ are used for offsets of word aligned data, 
           * resulting in [rN + ZeroExt(OFS_ << 2)]  */

      __asm volatile(
        "  MOV    r1,#4                \n\t"
        "  MOV    r0,LR                \n\t"
        "  ORR    r0,r0,r1             \n\t"
        "  MOV    LR,r0                \n\t"
       );
      REQUEST_PENDSV();
      break;

    default: // Unknown SVC request 
      break;
   }
  
} /* SVC_Handler_in_c */


/**************************************************************************
 * SVC_Handler(): System entering. Here extract SVCaller's stack address 
 */
__attribute__ ((used, naked)) void SVC_Handler(void) {

 // Which Stack was used when this interrupt happened 
  __asm volatile(
    "  MOVS   r0, #4               \n\t"
    "  MOV    r1, LR               \n\t"
    "  TST    r0, r1               \n\t"
   );
  __asm volatile(
    "  BEQ    .it_was_MSP          \n\t"
    "  MRS    R0, PSP              \n\t"  // The Stack used was PSP (Thread)
    "  B      .branch_to_c         \n\t"  // 16-bit Thumb, then B limit ±2KB
    ".it_was_MSP:                  \n\t"  
    "  MRS    R0, MSP              \n\t"  // The Stack used, was the MSP 

  ); 
  __asm volatile(
    ".branch_to_c:                 \n\t"
    "  LDR    R3,=.handler_in_c    \n\t"
    "  MOV    r2, #0x01            \n\t"  // bit[0] to 1 for Thumb code 
    "  ORR    r3, r2               \n\t" 
    "  BX     R3                   \n\t"  // JUMP, with no-return 
    :
    :
    : "r3", "cc"  
   );
} // SVC_Handler 




    /* -> Schedule, schedule-tool and SysTick -> */


/**************************************************************************
 * SysTick_Handler: Count Up seconds and start scheduler 
 */
void SysTick_Handler(void) {

 /* Update kernel-Up time, is also the system-clock */
  if (++kernel.time.ticks >= SYS_TICK_COUNTER) {
   /* Ticks is reset every second */ 
    kernel.time.ticks = 0;
    kernel.time.seconds++;
   }

 /* Is it scheduler-timer, due to sys-tick, only every 4 times */
  if ((0x03 & kernel.time.ticks) == 0x03) {
    __disable_irq();
    kernel.signals |= IRS_SIGNAL_SYSTICK;
    __enable_irq();  
   }

 /* Scheduler-timer on ALL events/signals */
  if ((IRS_SIGNAL_SYSTICK | IRS_SIGNAL_MASK) & kernel.signals)
    REQUEST_PENDSV();
} /* SysTick_Handler */


/* Inter-Process Communication(IPC): Search for pair(two blocked process's 
 * waiting for one another), one(1) set/par a the time. 
 */
__attribute__ ((used)) static void procBlockedTasks(void) {

 /* Is there a Sender (Massage or Value) */
  TCB *send_tcb = tcblink_pull_state(&(kernel.tcblink_block), STATE_BLOCKED_SENDER);
  if (send_tcb != TCB_NULL) {
   /* Is there a Receiver FOR THIS Sender (Massage or Value) */  
    TCB *recv_tcb = tcblink_pull_id_and(&(kernel.tcblink_block), (THREADID_T) send_tcb->sys.sender.destID, STATE_BLOCKED_RECEIVER);
    if (recv_tcb != TCB_NULL) {
     /* Is Sender sending an Massage (or just a Value) */
      int sentMassage = 0;
      if (send_tcb->sys.sender.massage_pp != 0) 
        sentMassage = (int) *send_tcb->sys.sender.signall_p;
     /*
      * Is the Receiver ready for an Massage-String
      */
      if (recv_tcb->sys.receiver.massageReady_pp != 0) { 
       /* Massage from Sender to Receiver, and is there buffer-room for it! */ 
        if ((sentMassage > 0) && (*recv_tcb->sys.receiver.signall_p > sentMassage)) {
         /* Tell Receiver the length of the Massage */
          *recv_tcb->sys.receiver.signall_p = sentMassage;
#ifndef STOP_IPC_MASSAGE_PUSH
         /* Push the data-massage over, with the ending ZERO(+1) and right now */ 
          memcpy(*recv_tcb->sys.receiver.massageReady_pp, *send_tcb->sys.sender.massage_pp, (size_t) sentMassage +1);
#else
         /* Or we transfer the pointer, to the sender's data buffer */
          *recv_tcb->sys.receiver.massageReady_pp = *send_tcb->sys.sender.massage_pp; 
#endif
         }
        else {
          if (sentMassage > 0) /* Massage too BIG */ 
            *send_tcb->sys.sender.signall_p = -1;
           /* No massage for Receiver this time */
            *recv_tcb->sys.receiver.massageReady_pp = 0;
           /* Tell Receiver about intented length of the Massage */
            *recv_tcb->sys.receiver.signall_p = *send_tcb->sys.sender.signall_p; 
         }
       } /* Massage */
     /*
      * A value/word ONLY, receiver wants a values/byte. 
      */
      else {
       /* Did Sender want to send an Aassage */
        if (sentMassage > 0) {   
          *send_tcb->sys.sender.signall_p = -1; /* Sender Error */
         /* Empty notification: The receiver receives a zero(0) */ 
          *recv_tcb->sys.receiver.signall_p = 0;
         } 
        else { 
#ifndef STOP_IPC_MASSAGE_PUSH
         /* Hand-over the value/word */
          *recv_tcb->sys.receiver.signall_p = *send_tcb->sys.sender.signall_p;
#else /* Same same */
          *recv_tcb->sys.receiver.signall_p = *send_tcb->sys.sender.signall_p;
#endif
        }
       } /* Value */

     /* Hand-Over sender Thread-ID. */
      *recv_tcb->sys.receiver.sourID_p = (THREADID_T) send_tcb->thread.info.tid;
     /* Shift both sender and receiver back to STATE_READY */
      recv_tcb->thread.info.state = STATE_READY;
      tcblink_push(&(kernel.tcblink_ready), recv_tcb);
      send_tcb->thread.info.state = STATE_READY;
      tcblink_push(&(kernel.tcblink_ready), send_tcb);
     } 
    else
     /* No receiver yet, restore sender */
      tcblink_push(&(kernel.tcblink_block), send_tcb);
   } /* A sender ! */

} /* procBlockedTasks */


/* Countdown the Delay-counter, when delay is over, send task back in
 * STATE_READY */
__attribute__ ((used)) static int procTaskDelayCounter(TCB *tcb) {

  register uint32_t countDown = tcb->sys.sleep.count;

 /* SYS_TICK_COUNTER is 4 times as fast as SYS_TICK_FREQ */
  if (countDown >= 4) 
    countDown -= 4;

  if (countDown < 4) {
    TCB *p = tcblink_pull_with_tcb(&(kernel.tcblink_sleep), tcb);
    if (p != TCB_NULL) {
      p->thread.info.state = STATE_READY;
      tcblink_push(&(kernel.tcblink_ready), p);
     }
   }
  else 
   /* Storage for future countdown */
    tcb->sys.sleep.count = countDown;
        
  return 0;
} /* procTaskDelayCounter */


/* Go through all Delayed(sleeping) threads, to see if it is time to 
 * wake-up */
__attribute__ ((used)) static void procDelayedTasks(void) {

  tcblink_list(&(kernel.tcblink_sleep), procTaskDelayCounter);
} /* procDelayedTasks */


/* Schedule */
__attribute__ ((used, aligned (4))) void schedule(void) asm (".schedule");

void schedule(void) {

  register uint32_t signalCopy;

 /* We are ALWAYS down here for a reason, something about the State on the current task. 
  * Maybe it's just been preempted by the SYSTICK or it want to be blocked(change State).
  * 
  * NOTE: Here we expect that: kernel.tcb_current != TCB_NULL
  */
  switch (kernel.tcb_current->thread.info.state) {

    case STATE_READY: /* Thread is preempted or yield */
      tcblink_push(&(kernel.tcblink_ready), kernel.tcb_current);
      break;
        
    case STATE_SLEEP:  /* Thread ask for pause/break */ 
      tcblink_push(&(kernel.tcblink_sleep), kernel.tcb_current);
      break;
        
    case STATE_BLOCKED_SENDER:
    case STATE_BLOCKED_RECEIVER:
      tcblink_push(&(kernel.tcblink_block), kernel.tcb_current);
      break;
/*        
Add blocked-receive with time-out 
      case STATE_BLOCKED_SEND_TIMEOUT:
      case STATE_BLOCKED_REC_TIMEOUT:
        tcblink_push(&(kernel.tcblink_block), kernel.tcb_current);
 and
        tcblink_push(&(kernel.tcblink_sleep), kernel.tcb_current);

        break;
*/
    default:
      // osErr = xxState;
      break;
  }


 /* Grip current SIGNALs */ 
  signalCopy = kernel.signals;


 /* Count-down: Process PAUSED or TIME-OUT tasks on SYSTICK 
  * NOTE: Yes, we-do-it-now, though there may be SIGNALs which 
  * also need handling.
  */
  if (IRS_SIGNAL_SYSTICK & signalCopy) {
   /* Before We process, We clear SYSTICK-signal */ 
    __disable_irq();
    kernel.signals &= ~IRS_SIGNAL_SYSTICK;
    __enable_irq();  
    signalCopy &= ~IRS_SIGNAL_SYSTICK;
    procDelayedTasks();
   }


 /* SIGNALs */
  if (IRS_SIGNAL_MASK & signalCopy) {

    TCB *recv_tcb = TCB_NULL;

    /*  LATER: If not first SIGNAL, THEN the next
     *  SIGNAL_LEN_T temp = signalCopy;
     */ 

   /* Extract the Signal, in priority order */
    signalCopy = osSignalGetNext(signalCopy);

   /* Find Thread-ID assigned to this Signal */ 
    THREADID_T threadAssigned = osSignalToThread(signalCopy); 
    if (threadAssigned) {
     /* Try to PULL it from BLOCKED_RECEIVER */
      recv_tcb = tcblink_pull_id_and(&(kernel.tcblink_block), threadAssigned, STATE_BLOCKED_RECEIVER);
      if (recv_tcb != TCB_NULL) {
       /* We have the one, now hand over parameters */
        *recv_tcb->sys.receiver.sourID_p = 0;   /* Always from, or via Kernel-ID(0) */ 
        *recv_tcb->sys.receiver.signall_p = signalCopy; 
       /* Is the receiver waiting for Massager ! */ 
        if (recv_tcb->sys.receiver.massageReady_pp != 0)
         /* Tell reciever, THIS IS A SIGNAL only */ 
          *recv_tcb->sys.receiver.massageReady_pp = 0; 

       /* Push the receiver-task in-to READY queue */ 
        recv_tcb->thread.info.state = STATE_READY;
        tcblink_push(&(kernel.tcblink_ready), recv_tcb);
       }
     } /* if (threadAssigned) */

   /* The Signal was used */
    if (recv_tcb != TCB_NULL) {
      osSignalClear(signalCopy);
      /* But We keep the NON-ZERO in: */
      /* signalCopy = 1;              */
     }
    else {
     /* if NOT Assigned: clear it, not to trigger the kernel again */
      if (!threadAssigned)
        osSignalClear(signalCopy);
      signalCopy = 0; 
     }
   } 
  
 /* INIT to READY: Start-Up delay before ISR SIGNAL handling */
  else if (IRS_SIGNAL_INIT & signalCopy) {
   /* Counting-down Start-Up delay */ 
    signalCopy &= IRS_SIGNAL_INIT_C_MASK;
    signalCopy--;

   /* Change state to READY */
    if (signalCopy == 0) {
      __disable_irq();
      kernel.signals &= ~(IRS_SIGNAL_INIT | IRS_SIGNAL_INIT_C_MASK);
      kernel.signals |= IRS_SIGNAL_READY;     
      __enable_irq();      
     }
    else {
     /* Store new count-down value */ 
      __disable_irq();
      kernel.signals &= ~IRS_SIGNAL_INIT_C_MASK;
      kernel.signals |= (IRS_SIGNAL_INIT_C_MASK & signalCopy);     
      __enable_irq(); 

      signalCopy = 0;
     }
   }
  else
    signalCopy = 0; 


 /* Inter-Process(Thread) Communication (IPC), NO SIGNALs */ 
  if (!signalCopy) /* equal to zero */
    procBlockedTasks();


 /* Next-IN-QUEUE, will now be current-task */
  TCB *next = tcblink_pull_with_priority(&(kernel.tcblink_ready));
  if (next != TCB_NULL) 
    kernel.tcb_current = next;
} /* schedule */



/* Handle Pending Request at lower interrupt priority, for schedule (context switching) */
__attribute__ ((used, naked)) void PendSV_Handler(void) {

  __asm volatile(
    "  MRS   r2,PSP                \n\t"
    "  SUB   r2,#32                \n\t"
    "  STMIA r2!,{r4-r7}           \n\t"
    "  MOV   r4,r8                 \n\t"
   );

  __asm volatile(
    "  MOV   r5,r9                 \n\t"
    "  MOV   r6,r10                \n\t"
    "  MOV   r7,r11                \n\t"
    "  STMIA r2!,{r4-r7}           \n\t"
   );

  __asm volatile(
    "  SUB   r2,#32                \n\t"
    "  LDR   r3,=.kernel_ptr       \n\t"
    "  LDR   r1,[r3,#0]            \n\t"
   ); 
  
  __asm volatile(
    "  LDR   r0,[r1,%0]            \n\t"
    "  STR   r2,[r0,%1]            \n\t"
    "  PUSH  {LR}                  \n\t"
    :
    : "I" (OFS_TCB_CURRENT), "I" (OFS_TCB_STACK)    
   );  /* NOTE: the "I" for constant 0..225 */


  __asm volatile(
    "  LDR    r3,=.schedule        \n\t"
    "  MOV    r2, #0x01            \n\t"  /* bit[0] to 1 for Thumb code */
    "  ORR    r3, r2               \n\t" 
    "  BLX    r3                   \n\t"
    :
    :
    : "r3", "cc"
   ); 

  
  __asm volatile(
    "  POP   {r0}                  \n\t"
    "  MOV   LR,r0                 \n\t"
    "  LDR   r3,=.kernel_ptr       \n\t"
    "  LDR   r1,[r3,#0]            \n\t"
   );

  __asm volatile(
    "  LDR   r0,[r1,%0]            \n\t"
    "  LDR   r2,[r0,%1]            \n\t"
    "  ADD   r2,#16                \n\t"
    "  LDMIA r2!,{r4-r7}           \n\t"
    :
    : "I" (OFS_TCB_CURRENT), "I" (OFS_TCB_STACK)    
   );

  __asm volatile(
    "  MOV   r8,r4                 \n\t"
    "  MOV   r9,r5                 \n\t"
    "  MOV   r10,r6                \n\t"
    "  MOV   r11,r7                \n\t"
   );

  __asm volatile(
    "  SUB   r2,#32                \n\t"
    "  LDMIA r2!,{r4-r7}           \n\t"
    "  ADD   r2,#16                \n\t"
    "  MSR   PSP,r2                \n\t"
   );

  
  // TODO:
  
  /* clear it or not ? */
  /*
  * If you use a preemptive kernel on ARM Cortex-M0/M3, perhaps you could check how your 
  * kernel handles PendSV. If you don’t see an explicit write to the PENDSVCLR bit, I would 
  * recommend that you think through the consequences of re-entering PendSV. I’d be very 
  * interested to collect a survey of how the existing kernels for ARM Cortex-M handle this 
  * situation.
  * 
  * http://embeddedgurus.com/state-space/2011/09/whats-the-state-of-your-cortex/
  */
  
  
  __asm volatile(
    "  BX    LR                    \n\t" 
   ); /* Branch to address contained in LR */ 
  
} /* PendSV_Handler */





   /* -> Thread Creation and Start-Up -> */


void osThreadCreate(THREADID_T threadID, void (*thread)(void), uint32_t stackSize, PRIORITY_T priority) {

  TCB *tcb = (TCB *) &tcb_list[threadID];
  CPUREGS *regs;

 // This is the button of THIS-ONE Stack 
  tcb->stack = lastStackInit;
 // Remember for the next init of Stack
  lastStackInit += stackSize;


#ifdef INITIALIZE_TEST_STACKS
 // Fill Task stack with nulls(0).
  int i;
  for (i = 0; i < stackSize; i++) {

#ifdef INITIALIZE_TASK_STACK_WITH
    tcb->stack[i] = (uint32_t) INITIALIZE_TASK_STACK_WITH;
#else
   // Fill with thread ID, while debugging
    tcb->stack[i] = (('0' + threadID) << 24) | (('0' + threadID) << 16) | (('0' + threadID) <<  8) | (('0' + threadID) <<  0);
#endif
   }
#endif

  tcb->stack += (stackSize - (TCB_HWSTACK_WORDSIZE + TCB_SWSTACK_WORDSIZE));

  tcb->prev = TCB_NULL;
  tcb->next = TCB_NULL;

 // Init Task with some success-level
  tcb->thread.info.success = 0x10;
  tcb->thread.info.priority = priority;
  tcb->thread.info.state = STATE_READY;
  tcb->thread.info.tid = threadID;

 // Init Task CPU
  regs = (CPUREGS *)(tcb->stack);
  regs->hw.r0 = 0x00;
  regs->hw.r1 = 0x00;
  regs->hw.r14_lr = 0x00;    // !!! TODO: Is this safe, ?make it reset?   
  regs->hw.r15_pc = (uint32_t)(thread);
  regs->hw.xpsr = 0x01000000;

 // Is it the kernel itself
  if (threadID == 0) {  /* KERNEL_ID = 0 */
    tcb->stack += TCB_SWSTACK_WORDSIZE;
    kernel.tcb_current = tcb;
   } 
  else
    tcblink_push(&(kernel.tcblink_ready), tcb);
    
} /* osThreadCreate */


/* Forward declaration; When no task(s) to do, sleep (power-down) */
__attribute__ ((used)) static void kernelSleep(void);

void osKernelCreate(void) {


#ifdef MONITOR_INTERRUPT_STACK /* WORK IN PROGRESS */

 /* Main Stack (HW), from the Linker */
  extern char __StackLimit[];
  extern char __StackTop[];

  uint32_t *hardStack = (uint32_t *) __StackLimit;
  int i;

  for (i = 0 ; i < (((__StackTop - __StackLimit) / 3) / 4); i ++) {
    hardStack[i]  = (('U') << 24) | (('U') << 16) | (('U') <<  8) | (('U') <<  0);
   }
  hardStack[i]  = (('V') << 24) | (('V') << 16) | (('V') <<  8) | (('V') <<  0);

#endif

 // No current Thread
  kernel.tcb_current = TCB_NULL;  
 // Init all possible Thread states  
  tcblink_init(&(kernel.tcblink_ready));
  tcblink_init(&(kernel.tcblink_sleep));
  tcblink_init(&(kernel.tcblink_block));
 // NO Deep-sleep or Power-down , only Sleep mode.
  LPC_PMU->PCON |= PMU_PCON_NODPD;

 // The kernel is our first "Thread", Thread #0, priority 1(ULTRA LOW). 
  osThreadCreate(0, kernelSleep, KERNEL_STACK, 1);
} /* osKernelCreate */



/* TODO */
// void osTickTimerCalib(void) {
// }


/* Get it started */
void osKernelRun(void) {

 /* INIT ISR-signals: Delay start before Ready. */
  kernel.signals = (NUM_OF_THREAD & IRS_SIGNAL_INIT_C_MASK);
  kernel.signals |= IRS_SIGNAL_INIT;
 /* Set kernel clock, get the first time from board-RTC */
  kernel.time.ticks = 0;   
  kernel.time.seconds = 0; 

 /* SVC and Pending Interrupt Request have the lowest priority, and the 
  * same priority due to limitations in Cortex-m0 */
  NVIC_SetPriority(SVCall_IRQn, 3);
  NVIC_SetPriority(PendSV_IRQn, 3);
 /* We give the SysTick a little-bit low priority, BUT NOT THE LOWEST.
  * ADC and special times have the highest priority, UARTS are medium. */ 
  NVIC_SetPriority(SysTick_IRQn, 2); 
  
 /* Get the factual core clock */
  microAmountOfClocks = SystemCoreClock;
 /* The Tick-Timer is manually initialisation, to make sure */
  SysTick->VAL = 0UL;
 /* If SysCoreClock runs faster than 16.7MHz, then let SysTick-Counter 
  * run half the speed. */
  if (microAmountOfClocks > 0x0FFFFFFUL) {
   /* Div 2 */ 
    microAmountOfClocks = (microAmountOfClocks >> 1); 
   /* Set Reload register */ 
    SysTick->LOAD  = (uint32_t)((microAmountOfClocks / SYS_TICK_COUNTER) -1UL);  
   /* Enable IRQ and run timer at half the SysCoreClock */
    SysTick->CTRL  = SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk; 
   }
  else {
   /* Set Reload register */ 
    SysTick->LOAD  = (uint32_t)((microAmountOfClocks / SYS_TICK_COUNTER) -1UL);
   /* Run directly on SysCoreClock, enable IRQ and run timer */
    SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
   }

 /* Now store the amount of sysTicks for one microsecond */
  microAmountOfClocks = microAmountOfClocks / 1000000UL;
  
 /* Run schedule for the first time */
  SVC(SVCFUNC_START);

 /* -> Shouldn't be here, the watchdog timer will handle this. */
  // osErr = xxState;
  
} /* osKernelRun */




/**************************************************************************
 * KernelSleep:
 * When nothing better to do, like handing-over resources to the next thread, 
 * the Kernel itself fall to sleep until next HW-interrupt. 
 */
__attribute__ ((used)) static void kernelSleep(void) {

  while (1) {
 
   /* If everything(all threads) is event driven, we could here, have some 
    * system-monitoring, such as temperature measurements and check of Stack.
    */
    
   // Enter MCU Sleep mode
    Chip_PMU_SleepState(LPC_PMU);
   }
} /* kernelSleep */

