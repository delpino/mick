/* -> Here just a example with IO Tasks, SIGNALs and a application */

/**************************************************************************
 *                                    Link: www.linkedin.com/in/kentdelpino
 *                                    Author: Kent DEL PINO
 * File:      _DEF_YOUR_TASKS.h
 * Used with: GNU Arm Embedded Toolchain, see:
 *            https://developer.arm.com/open-source/gnu-toolchain/gnu-rm
 * Version:   0.4.2
 * Date:      1 FEB. 2018
 *
 * D E C L A R A T I O N S  O F:
 *    - Threads/Tasks to run on the MICK-scheduler, their priority and Stack 
 *      size.
 *    - IRS_SIGNAL handlers/routing 
 *    
 *
 *  -- This source-code is released into the public domain, by the author --
 */

#ifndef DEF_YOUR_TASKS_H
#define DEF_YOUR_TASKS_H

#ifdef __cplusplus
extern "C" {
#endif


/* Task included header-files. Remember to also add them to makefile */
#include "task_stdin_json.h"
#include "task_stdout_json.h"
#include "_our_APP.h"



/* Define each thread's/task's ID here. Each thread with a unique number 
 * from 1 and consecutively up, and with a priority greater than 1 (higher 
 * number, HIGHER PRIORITY). The Kernel itself has ID 0 and a sleep-priority 
 * equal to 1 (very-very low), this is NOT to be changed.
 * 
 * The third number We need to define is the stack size of each thread/task.
 * Remember the stack size is in words of 4-bytes. 
 * 
 *  * NOTE: Stack used by IAP commands can be up-to 148 bytes/37 words. 
 * 
 * And then, at last, update SYS_STACK_NEEDED, the sum of all Stacks.
 * 
 * Communications input-Threads should have high priority. 
 */

/* Thread-ID */
enum TID {
     KERNEL_ID,         /* First, is #0, always named KERNEL_ID */
  STDIN_JSON_ID,
  STDOUT_JSON_ID,
 
  OUR_APP_ID,
  /*MAYBE_MORE_ID,*/

     NUM_OF_THREAD      /* MUY IMPORTANTE. Last, is always NUM_OF_THREAD */ 
};

/* Thread priority; 64 is very high, 4 is extremely low. Keep the numbers 
 * between 4 and 64. 
 */
enum TPRIORITY {
     KERNEL_SLEEP = 1,   /* Kernel power-down priority, always low (1)  */     
  STDIN_JSON_PRIO = 64,   /* Principle, Higher prio for input handlers  */
  STDOUT_JSON_PRIO = 64,  /* Principle, Lower prio for output           */
                          /* DO NOT LET A HIGH PRIO WAIT FOR A LOW PRIO */ 
  OUR_APP_PRIO = 12,
  /*MAYBE_MORE_PRIO = 12,*/ 
};

/* Stack size in WORDs(4 bytes) for the individuel threads. Min. 16 + 4 */
enum TSTACK {
    KERNEL_STACK = 32 +4,   /* (+4) Kernel-sleep-mode, it don't do much */
  STDIN_JSON_STACK = 192,   /* Good size, for JSON-obj input handling.  */
  STDOUT_JSON_STACK = 192,  /* IO-formatting text, good Stack is needed.*/

  OUR_APP_STACK = 320,
  /*MAYBE_MORE_STACK = 128,*/
};

/* Warning: It is of course important to get the following right. */ 
#define OS_STACK_NEEDED (KERNEL_STACK + STDIN_JSON_STACK + STDOUT_JSON_STACK + OUR_APP_STACK)  /* + MAYBE_MORE_STACK */



/* Define Event(signals)-handlers.
 *
 * Signal-Flags is the simplest way which we can tell a Thread/Task to 
 * take action, based on events. Example, a data-frame received on PHY 
 * layer in the background, by a Interrupt Service Routine(ISR), let's say 
 * on TTYS0/STDIN, can be further processed by a specific handler-Thread.
 *
 * Event-types are fixed in this system (almost hard-wired), here you can 
 * point to Handlers(a Task ID), Not used Signals must be null(0).
 * 
 * This Event handling system is best for data package, it is not good for  
 * fast moving small bits and bytes. See more in mich.h (SIGNAL constants)
 */

enum THANDLERS {             /* Warning: alot of Hard-Wired stuff */
/*                  = ID_OF_THREAD    */
  SIGNAL_HANDLER_00 = 0,               /*  ISR_SIGNAL_ABSOLUTE   */
  SIGNAL_HANDLER_01 = 0,               /*  ISR_SIGNAL_ANALOG     */
  SIGNAL_HANDLER_02 = OUR_APP_ID,      /*  ISR_SIGNAL_TIMER      */
  SIGNAL_HANDLER_03 = 0,               /*  ISR_SIGNAL_POWER_CTRL */
/* Input of serial data package. */
  SIGNAL_HANDLER_04 = 0,               /*  ISR_SIGNAL_SPI_RXDATA */
  SIGNAL_HANDLER_05 = STDIN_JSON_ID,   /*  IRS_SIGNAL_TTYS0_RXD  */
  SIGNAL_HANDLER_06 = 0,               /*  IRS_SIGNAL_UARTS_RXD  */
  SIGNAL_HANDLER_07 = 0,               /*  ISR_SIGNAL_I2C_RXDATA */ 
/* Notifications to application levels, who waits the package.*/
  SIGNAL_HANDLER_08 = STDOUT_JSON_ID,  /*  WHO_TO_DELIVER_TO_0   */
  SIGNAL_HANDLER_09 = 0,               /*  WHO_TO_DELIVER_TO_1   */
  SIGNAL_HANDLER_10 = 0,               /*  WHO_TO_DELIVER_TO_2   */
  SIGNAL_HANDLER_11 = OUR_APP_ID,      /*  WHO_TO_DELIVER_TO_3   */
/* Output, package sent. */
  SIGNAL_HANDLER_12 = 0,               /*  ISR_SIGNAL_SPI_COMP   */
  SIGNAL_HANDLER_13 = 0,               /*  IRS_SIGNAL_TTYS0_COMP */
  SIGNAL_HANDLER_14 = 0,               /*  IRS_SIGNAL_UARTS_COMP */
  SIGNAL_HANDLER_15 = 0,               /*  ISR_SIGNAL_I2C_COMP   */
};




#ifdef __cplusplus
}
#endif

#endif /* DEF_YOUR_TASKS_H */
